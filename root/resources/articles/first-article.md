The website's up!

I have this neat template engine that compiles blog articles for me, but it's
not seeing much use right now because I'm short on time to actually write
anything.  So, here's a list of some planned articles I'll write in the future:

- Building and Programming a [Remote Control Snow Plow](https://www.youtube.com/watch?v=lfSqagByDVk)
- Installing and Configuring [resty-gitweb](https://git.joshstock.in/resty-gitweb) for Production
- Building nginx With OpenResty
- Generating Websites With Template Engines in Python
- Using OpenResty To Write Dynamic Websites
- My Website Stack
- Using ESP32 to Read a Radio Receiver's PWM Data
- Establishing UART Communication With ESP32
- Implementing [Minesweeper in C with ncurses](https://git.joshstock.in/ncurses-minesweeper) (Game Programming in C)
- My Philosophy on Personal Task Management

There's also a significant amount of work I still have to do on this website.
Stay tuned!
